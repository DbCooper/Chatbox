var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
var FacebookStrategy = require("passport-facebook").Strategy;
var config 		= require('../config');
var userRepository = require("../repository/userRepository");
var User = require("../models/user");
var init = () => {

    passport.serializeUser((user,done) => {
        done(null,user.id);
    });

    passport.deserializeUser(async (id,done) => {
        var user =await userRepository.findById(id);
        done(null,user);
    });

    passport.use("login-local",new LocalStrategy((username, password, done)=> {

        console.log(username,password);
        var user = async () => {
            var dataReturn = await userRepository.findOne({'username': RegExp(username, 'i')}, {'socialId': null});
            if (!dataReturn){
                return done(null,false,{message: 'Incorrect username or password.' });
            }
            console.log("Data return: "+dataReturn.username);
            dataReturn.validatePassword(password, function(err, isMatch) {
                if (err) { return done(err); }
                if (!isMatch){
                    return done(null, false, { message: 'Incorrect username or password.' });
                }
                return done(null, dataReturn);
            });
        }
        user().catch((e) => console.log(e));
    }));

    var verifySocialAccount =async (tokenA, tokenB, data, done) => {
        var user = await userRepository.findOrCreate(data);
        return done(null,user);
    }

    // Plug-in Facebook & Twitter Strategies
    passport.use(new FacebookStrategy(config.facebook, verifySocialAccount));

    return passport;
};

module.exports = init();
