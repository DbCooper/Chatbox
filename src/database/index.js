var Mongoose = require("mongoose");
var config = require("../config");

var ConnectData = async () =>{
    let dbURI = "mongodb://" +
        //encodeURIComponent(config.db.username) + ":" +
        //encodeURIComponent(config.db.password) + "@" +
        config.db.host + ":" +
        config.db.port + "/" +
        config.db.name;

    try {
        await Mongoose.connect(dbURI);
        console.log("Connect MongoDb Success!!");
    }catch (e) {
        console.log("Connect MongoDb Error: "+e.message);
        throw e;
    }
};

module.exports = ConnectData;