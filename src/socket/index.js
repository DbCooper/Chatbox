var config = require('../config');
var redis = require('redis').createClient;
var adapter = require('socket.io-redis');
var storeRedis = require('redis');
var RoomRepository = require('../repository/roomRepository');
var UserRepository = require('../repository/userRepository');
var MessageRepository = require('../repository/messageRepository');

var client = '';
var init = app => {
    var server = require('http').Server(app);
    var io = require('socket.io')(server);
    io.set('transports', ['websocket']);

    let port = config.redis.port;
    let host = config.redis.host;
    let password = config.redis.password;
    let pubClient = redis(port, host, {auth_pass: password});
    let subClient = redis(port, host, {auth_pass: password, return_buffers: true,});
    io.adapter(adapter({pubClient, subClient}));

    console.log("redis host: " + host + " port: " + port);
    io.use((socket, next) => {
        require('../session')(socket.request, {}, next);
    });
    client = storeRedis.createClient('redis://' + config.redis.user + ':' + config.redis.password + '@' + host + ':' + port);

    client.del('user_online');
    ioChatServer(io);

    return server;
};
var getUserStatus = async (userActive) => {
    let currentUserStatus = []
    let allUser = await UserRepository.findAll();
    allUser.forEach(x => {
        var flag = false;
        userActive.forEach(uo => {
            if (uo._id === x._id) {
                flag = true;
            }
        });
        if (flag) {
            currentUserStatus.push({
                x,
                status: 'online'
            });

        } else {
            currentUserStatus.push({
                x,
                status: "offline"
            })
        }
    });
    return currentUserStatus;
};
var ioChatServer = (io) => {
    io.of("/rooms").on('connection', socket => {
        socket.on('createRoom', async (title, description) => {
            let isRoomValid = await RoomRepository.findOne({'title': new RegExp('^' + title + '$', 'i')});
            if (isRoomValid) {
                socket.emit('updateRoomList', {error: 'Room title already exists.'});
            } else {
                let newRoom = {
                    title: title,
                    description: description
                };
                newRoom = await RoomRepository.create(newRoom);
                socket.emit('updateRoomList', newRoom);
                socket.broadcast.emit('updateRoomList', newRoom);
                console.log('Broadcast success');
            }
        });
    });

    io.of('/chatroom').on('connection', socket => {
        socket.on('user_online', async () => {
            if (socket.request.session.passport == null) {
                return
            }
            let userId = socket.request.session.passport.user;
            let chanelPrivate = userId + '_private';
            socket.join(chanelPrivate);
            let userToRedis = await UserRepository.findById(userId);
            client.sadd('user_online', JSON.stringify(userToRedis));
            let userOnline = [];
            client.smembers('user_online', async (err, replies) => {
                if (err) throw err;
                replies.forEach(x => userOnline.push(JSON.parse(x)));
                let currentUserStatus = await getUserStatus(userOnline);
                console.log(currentUserStatus);
                socket.emit('statusUser', currentUserStatus);
                socket.broadcast.emit('statusUser', currentUserStatus);
            });

        });

        socket.on('join', async roomId => {

            let isRoomValid = await RoomRepository.findById(roomId);
            console.log('Room Id: ', roomId);
            if (!isRoomValid) {
                console.log('Room not exist');
                socket.emit('updateCurrentRoom', {error: 'Room doesnt exist.'});
            } else {
                if (socket.request.session.passport.user == null) {
                    return;
                }
                await RoomRepository.addUser(isRoomValid, socket);
                let newRoom = await RoomRepository.findById(roomId);
                console.log("Room is: " + newRoom);
                socket.join(roomId);
                let userInRoom = await RoomRepository.getUsers(newRoom, socket);
                let messageNew = await MessageRepository.findMessage({'recipientId': roomId});
                socket.emit('updateCurrentRoom', {users: userInRoom, messages: messageNew});
                socket.broadcast.to(roomId).emit('updateCurrentRoom', {
                    users: userInRoom,
                    messages: messageNew
                });
            }


        });

        socket.on('loadroom', async () => {
            let allRoom = await RoomRepository.find();
            let reviewRoom = [];
            allRoom.forEach(x => {
                reviewRoom.push({
                    _id: x._id,
                    title: x.title,
                    totalUser: x.connections.length
                })
            });
            socket.broadcast.emit('totalRoom', reviewRoom);
            socket.emit('totalRoom', reviewRoom);
        });

        socket.on("leaveroom", async (roomId) => {
            if (socket.request.session.passport.user == null) {
                socket.emit("updateLeaveRoom", {error: "Room doest exist."});
                return;
            }
            let dataLeaveRoom = await RoomRepository.removeUser({'_id': roomId}, socket);
            socket.broadcast.to(roomId).emit("updateLeaveRoom", {data: dataLeaveRoom});
            socket.emit("updateLeaveRoom", {data: dataLeaveRoom});
        });

        socket.on('send', async (message, roomId) => {
            console.log('Message: ' + message + 'room:' + roomId);
            let userId = socket.request.session.passport.user;

            console.log(userId);
            var messageObj = {
                senderId: userId,
                recipientId: roomId,
                messageText: message
            };
            messageObj = await MessageRepository.createMessage(messageObj);
            console.log(messageObj);
            socket.emit('addMessage', messageObj);
            socket.broadcast.to(roomId).emit('addMessage', messageObj);
        });

        socket.on('disconnect', async () => {
            if (socket.request.session.passport == null) {
                return;
            }
            let userId = socket.request.session.passport.user;
            console.log(userId);
            var userDisConnect = await UserRepository.findById(userId);
            client.srem('user_online', JSON.stringify(userDisConnect));

            client.smembers('user_online', async (err, replies) => {
                if (err) throw err;
                let userOnline = [];
                replies.forEach(x => userOnline.push(JSON.parse(x)));
                userOnline.forEach(x => console.log(x.username));
                let currentUserStatus = await getUserStatus(userOnline);
                socket.broadcast.emit('statusUser', currentUserStatus);
            });
        });

        socket.on('private_chat', async (message, recipientId) => {
            let isUserValid = await UserRepository.findById(recipientId);
            if (!isUserValid) {
                console.log("User not exist database");
                socket.emit('sendMessagePrivate', {error: 'User not exist'});
            } else {
                var userId = socket.request.session.passport.user;
                var messageNew = {
                    senderId: userId,
                    recipientId: recipientId,
                    messageText: message
                };
                messageNew = await MessageRepository.createMessage(messageNew);
                console.log('Message New:' + messageNew);
                socket.broadcast.to(recipientId + "_private").emit('sendMessagePrivate', messageNew);
                socket.emit('sendMessagePrivate', messageNew);
            }
        });

        socket.on('load_private_message', async recipientId => {
            let isUserValid = await UserRepository.findById(recipientId);
            console.log(recipientId);
            if (!isUserValid) {
                console.log("User not exist database");
                socket.emit('loadPrivateMessage', {error: 'User not exist'});
            } else {
                var userId = socket.request.session.passport.user;
                console.log('userId: '+userId+" reciId: "+recipientId);
                let preMessage = await MessageRepository.findMessage({senderId: userId, recipientId: recipientId});
                let oldMessage = await MessageRepository.findMessage({senderId: recipientId, recipientId: userId});

                let allMessage = sortBy(preMessage.concat(oldMessage), {
                    props: 'createAt',
                    desc: true,
                    parser: function (item) {
                        return new Date(item);
                    }
                });

                socket.emit('loadPrivateMessage', allMessage);
            }
        })
    });
};
var sortBy = (function () {
    var toString = Object.prototype.toString,

        parse = function (x) {
            return x;
        },

        getItem = function (x) {
            var isObject = x != null && typeof x === "object";
            var isProp = isObject && this.prop in x;
            return this.parser(isProp ? x[this.prop] : x);
        };

    return function sortby(array, cfg) {
        if (!(array instanceof Array && array.length)) return [];
        if (toString.call(cfg) !== "[object Object]") cfg = {};
        if (typeof cfg.parser !== "function") cfg.parser = parse;
        cfg.desc = !!cfg.desc ? -1 : 1;
        return array.sort(function (a, b) {
            a = getItem.call(cfg, a);
            b = getItem.call(cfg, b);
            return cfg.desc * (a < b ? -1 : +(a > b));
        });
    };

}());

module.exports = init;