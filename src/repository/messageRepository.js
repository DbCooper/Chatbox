var Message = require("../models/message");

var MessageRepository = {};

/*
* Tạo mới tin nhắn
* */
MessageRepository.createMessage = async message =>{
    try {
        let messageNew = new Message();
        messageNew.senderId = message.senderId;
        messageNew.recipientId = message.recipientId;
        messageNew.messageText = message.messageText;
        return await messageNew.save({_id:false});
    }catch (e) {
       console.log(e);
    }
};
/*
* Tìm kiếm tin nhắn theo sendId và recipientId
* */
MessageRepository.findMessage = async (data) => {
    try {
        let messages =await Message
            .find(data)
            .limit(10)
            .sort({'createAt': 'desc'});
        console.log("Find message success");
        return messages.reverse();
    }catch (e) {
        console.log(e);
    }
};

module.exports =MessageRepository;
