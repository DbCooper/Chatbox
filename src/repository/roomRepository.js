var Room = require("../models/rooms");
var User = require("../models/user");
var Mongoose = require("mongoose");
var Message = require("../models/message")
var RoomRepository = {};

RoomRepository.create = async (room) => {
    try {
        let newRoom = new Room();
        newRoom.title = room.title;
        newRoom.description = room.description;

         return await newRoom.save({_id:false});

    }catch (e) {
        console.log("Error: "+e.message);
    }
};

RoomRepository.find = async (predicate) => {
    try {
        let dataReturn = await Room.find(predicate);
        console.log("find the data: "+dataReturn);
        return dataReturn;
    }catch (e) {
        console.log("Error: "+e.message);
    }
};

RoomRepository.findOne = async (predicate) => {
    try {
        let dataReturn = await Room.findOne(predicate);
        console.log("find one data success");
        return dataReturn;
    }catch (e) {
        console.log("Error: "+e.message);
    }
};

RoomRepository.findById = async (id) => {
    try {
        let dataReturn = await Room.findById(id);
        console.log("find by id success");
        return dataReturn;
    }catch (e) {
        console.log("Error: "+e.message);
    }
};

//* Thêm người dùng cùng với socket tương ứng vào phòng đã qua.
RoomRepository.addUser = async (room,socket) => {
    try {
        let userId = socket.request.session.passport.user;;
        let isUserInRoom = await Room.findOne({'connections.userId': userId});
        if (isUserInRoom) {
            console.log("User is already in this room.")
            await Room.update({"_id": isUserInRoom._id,"connections.userId":userId},{
                $set:{
                    "connections.$.socketId": socket.id
                }
            });
            console.log("Update socket for user success !!!");
            return ;
        }
        let conn = {
            userId: userId,
            socketId: socket.id
        };

        await Room.update({"_id":room._id},{
            $push:{
                'connections': conn
            }
        });
        console.log("Add user success");


    }catch (e) {
        console.log("Error: "+e.message);
    }
};

//Get tất cả người dùng trong một room;
RoomRepository.getUsers =async (room, socket) =>{
    try {
        let users = [], vis = {}, cunt = 0;
        let userId = socket.request.session.passport.user;

        room.connections.forEach(rc => {
            if (rc.userId !== userId) {
                cunt++;
            }
            ;
            if (!vis[rc.userId]) {
                users.push(rc.userId);
            }
            vis[rc.userId] = true;
        });

        let allUser = await User.find();
        let userInRoom = allUser.filter(us => users.find(x => x === us._id));
        return {
            users: userInRoom,
            cunt: cunt
        };
    }catch (e) {
        console.log("Get user failed!!"+e.message);
    }
};

//Xoá người dùng khi người dùng nhấn thoát hẳn cuộc trò chuyện
RoomRepository.removeUser =async (room, socket) =>{
    try {
        let pass = true, cunt = 0, target = 0;
        let userId = socket.request.session.passport.user;;
        let roomFinded = await Room.find(room);
        roomFinded.connections.forEach(rc => {
            if (rc.userId !== userId){
                cunt++;
            };
            if (rc.socketId === socket.id){
                pass = false;
                target = rc._id;
            }
        });
        if(!pass) {
            roomFinded.connections.id(roomFinded.connections[target]._id).remove();
            await roomFinded.save();
            return {
                cunt: cunt,
                room: roomFinded,
                userId: userId
            }
        }

        return pass;
    }catch (e) {
        console.log("Remove user in room failed!!"+e.message);
    }
};

module.exports = RoomRepository;
