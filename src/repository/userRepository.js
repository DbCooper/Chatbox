var User = require("../models/user");
var Mongoose = require("mongoose");

var UserRepository = {};

UserRepository.create = async (user) => {
    try {
        var newUser = new User();

        newUser.username = user.username;
        newUser.password = user.password;
        await newUser.save({_id:false});
        console.log("Create user success");
    }  catch (e) {
        console.log("Create user error");
        throw e;
    }
};

UserRepository.findAll =async () =>{
    try {
        return await User.find({});
    }catch (e) {
        console.log("Find all error: "+e.message);
    }
}

UserRepository.findOne = async (data) => {
    try {
        var user =await User.findOne(data);
        console.log("Find one success");
        return user;
    }  catch (e) {
        console.log("Find one error");
        throw e;
    }
};

UserRepository.findById = async (id) => {
    try {
        var user =await User.findById(id);
        console.log("Find by id  success");
        return user;
    }  catch (e) {
        console.log("Find by id error");
        throw e;
    }
};

/**
* Tìm người dùng và tạo một người dùng nếu chưa tồn tại.
* Phương thức này chỉ được sử dụng để tìm tài khoản người dùng đã đăng ký qua Xác thực xã hội.
*
*/
UserRepository.findOrCreate = async (data) => {
    try {
        let user =await User.findOne({socialId: data.id});
        if (!user){
            var userData =new User();
            userData.username = data.displayName;
            userData.socialId = data.id;
            userData.picture = data.photos[0].value || null;
            if (data.provider = "facebook" && data.picture){
                userData.picture = "http://graph.facebook.com/" + data.id + "/picture?type=large";
            }
            userData = await userData.save();
            console.log("Create user social success!!");
            return userData;
        }else{
            return user;
        }
    }catch (e) {
        console.log("Find or create Social Error: "+e.message);
        throw e;
    }
};

module.exports = UserRepository;