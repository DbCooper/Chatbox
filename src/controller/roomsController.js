var RoomRepo = require("../repository/roomRepository");

var Rooms = {};

Rooms.Listroom = async (req, res, next) =>{
    let allRoom = await RoomRepo.find();
    console.log(req.user);
    res.render('rooms',{rooms: allRoom,username: req.user.username});
};

module.exports = Rooms;