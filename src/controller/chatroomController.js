var RoomRepository = require("../repository/roomRepository");
var UserRepository = require("../repository/userRepository");
var ChatRoomController ={};

ChatRoomController.Index =async (req,res,next) => {
    var user = req.user;
    if (!user){
        res.redirect('/rooms');
    }
    res.render('chatroom',{user: user});
};

module.exports = ChatRoomController;
