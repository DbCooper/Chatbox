var Mongoose = require("mongoose");
var bcrypt = require("bcrypt-nodejs");
var Rd = require("random-number");


const SALT_WORK_FACTOR = 10;
const DEFAULT_USER_PICTURE = "https://bootdey.com/img/Content/avatar/avatar1.png";

var UserSchema = new Mongoose.Schema({
    _id:{ type: Number },
    username: { type: String, required: true},
    password: { type: String, default: null },
    socialId: { type: String, default: null },
    picture:  { type: String, default:  DEFAULT_USER_PICTURE}
},{_id: false });

UserSchema.pre("save",function(next) {
    var u = this;
    if (u.picture === null){
        u.picture = DEFAULT_USER_PICTURE;
    };
    var option = {
        min: 1000,
        max: 100000,
        integer: true
    };
    u._id = Rd(option);
    if (!u.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(u.password, salt, null, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            u.password = hash;
            next();
        });
    });
});

UserSchema.methods.generatePassword = password => bcrypt.hashSync(password,bcrypt.genSaltSync(8),null);

//UserSchema.methods.validatePassword = password => bcrypt.compareSync(password,this.password);

UserSchema.methods.validPassword = password => bcrypt.compareSync(password,this.password);

UserSchema.methods.validatePassword = function(password, callback) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return callback(err);
        callback(null, isMatch);
    });
};
module.exports = Mongoose.model("User",UserSchema);