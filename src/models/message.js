var Mongoose = require("mongoose");
var Rd = require("random-number");

var MessageSchema =new Mongoose.Schema({
    _id: { type: Number },
    senderId: { type: Number, required: true },
    recipientId: { type: Number, required: true },
    messageText: { type: String, required: true },
    createAt: { type: Date, default: Date.now }
});

MessageSchema.pre("save",function (next){
    let m = this;
    var option = {
        min: 1000,
        max: 100000,
        integer: true
    };
    m._id = Rd(option);
    return next();
});

module.exports = Mongoose.model("Message",MessageSchema);