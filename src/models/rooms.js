var Mongoose = require("mongoose");
var Rd = require("random-number");

var RoomSchema = new Mongoose.Schema({
    _id: { type: Number },
    title: { type: String, required: true },
    description: { type:String, required: true },
    connections:{ type: [{ userId: Number, socketId: String }]}
},{ _id: false });

RoomSchema.pre("save", function(next){
    var r = this;
    if(r._id != null){
        let option = {
            min: 1000,
            max: 100000,
            integer: true
        };
        r._id = Rd(option);
    }
    return next();
});


module.exports = Mongoose.model("Rooms",RoomSchema);