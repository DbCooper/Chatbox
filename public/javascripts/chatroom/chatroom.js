var chatroom = {
    chat: (username) => {
        var socket = io('/chatroom', {transports: ['websocket']});
        socket.on('connect', () => {
            socket.emit('user_online');

            socket.on('statusUser', users => {
                var html = '';
                $.each(users, (i, user) => {

                    html +=
                        '<div class="row sideBar-body starting-chat-private" data-id="' + user.x._id + '">' +

                        '<div class="col-sm-3 col-xs-3 sideBar-avatar">' +
                        '<div class="avatar-icon">' +
                        '<img src="' + user.x.picture + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-9 col-xs-9 sideBar-main">' +
                        '<div class="row">' +
                        '<div class="col-sm-8 col-xs-8 sideBar-name">' +
                        '<span class="name-meta">' + user.x.username + '' +
                        '</span>' +
                        '<div class="status">' +
                        '<i class="fa fa-circle ' + user.status + '"></i>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';

                })
                $('#tab_default_1').html(html);
            });

            socket.emit('loadroom');

            socket.on('totalRoom', rooms => {
                console.log(rooms);
                let html = '';

                html += '<ul class="list-group">';

                $.each(rooms, (i, room) => {
                    html += '<li class="list-group-item starting-chat-room" data-type="chat_room" data-id="' + room._id + '">' + room.title + ' <span class="badge" style="width: auto !important;">' + room.totalUser + '</span></li>';
                });
                html += '</ul>'
                $("#tab_default_2").html(html);
            });

            $('.container').on('click', '.starting-chat-room', function (evt) {
                var ele = $(this);
                var roomId = $(ele).attr('data-id');
                $('#type_chat').val(1);
                $("#room_chat_id").val(roomId+'-room');

                socket.emit('join', roomId);
            })

            socket.on('updateCurrentRoom', data => {
                if (data.users != null) {
                    console.log("user online:" + data.users);
                }
                ;
                var html = '';
                if (data.messages.length > 0) {
                    var messages = data.messages;

                    $.each(messages, (i, message) => {
                        html+= showMessage(message);
                    });

                }
                $('#conversation').html(html);
                $('.key-messages').css('display', 'block');
            });

            $(".reply-send").click(evt => {
                var text = $("#comment").val().trim();
                var roomId = $("#room_chat_id").val().split('-')[0];
                var typeChat = $('#type_chat').val();
                switch (typeChat) {
                    case '1':
                        if
                        (text.length > 0)
                        {
                            socket.emit('send', text, roomId);
                        }
                        break;
                    case '2':
                        if
                        (text.length > 0)
                        {
                            socket.emit('private_chat', text, roomId);
                        }
                        break;
                    default:
                        alertDialog('Chat','not type chat','error');
                }
            });

            socket.on('addMessage', message => {

                var html = showMessage(message);
                $('#conversation').append(html);
            });

            $('.container').on('click','.starting-chat-private',function(evt){
                var ele = $(this);
                let recipientId = $(ele).attr('data-id');
                $('#room_chat_id').val(recipientId+'-private');
                $('#type_chat').val(2);
                console.log(recipientId);
                socket.emit('load_private_message',recipientId);
            });

            socket.on('loadPrivateMessage',messagePrivate =>{
                console.log(messagePrivate);
               if (messagePrivate.length >0){
                   var html = '';
                   $.each(messagePrivate,(i,messages)=>{

                           html+=showMessage(messages);

                   });
                   $('#conversation').html(html);
               }
                $('.key-messages').css('display', 'block');
            });

            socket.on('sendMessagePrivate',messagePrivate =>{
                let roomsId= $('#room_chat_id').val();
                let userId= $('#user_id').val();
                let roomA = messagePrivate.senderId+'-private';
                let roomB = messagePrivate.recipientId+'-private';
                if(roomsId===roomA || roomsId === roomB){
                    var html = showMessage(messagePrivate);
                    $('#conversation').append(html);
                }else{
                    if (userId !== messagePrivate.senderId){
                        var html = '<div class="notice notice-success">'+
                            '<strong>Bạn có tin nhắn mới từ: '+messagePrivate.senderId+'</strong>'+
                            messagePrivate.messageText+
                            '</div>';
                        $('#notice-show').html(html);
                    }
                }
            })

        });
    }
};
var showMessage = (message) => {
    var userId = $('#user_id').val();
    var html = '';
    if (message.senderId === parseInt(userId)) {
        html += '<div class="row message-body">' +
            '<div class="col-sm-12 message-main-sender">' +
            '<div class="sender">' +
            '<div class="message-text">' +
            message.messageText +
            '</div>' +
            '<span class="message-time pull-right">' +
            message.createAt +
            '</span>' +
            '</div></div></div>';
    } else {
        html += '<div class="row message-body">' +
            '<div class="col-sm-12 message-main-receiver">' +
            '<div class="receiver">' +
            '<div class="message-text">' +
            message.messageText +
            '</div>' +
            '<span class="message-time pull-right">' +
            message.createAt +
            '</span>' +
            '</div></div></div>';
    }
    return html;
}