
var rooms = () =>{
    var socket = io('/rooms',{transports: ['websocket']});
    socket.on('connect', () => {
        socket.on('updateRoomList', room => {
            console.log(room);
            if (room.error != null){
                alertDialog('Create Room',room.error,'error');
            } else {
                let html = '<tr>' +
                    '<td>' + room._id + '</td>' +
                    '<td>' + room.title + '</td>' +
                    '<td>' + room.description + '</td>' +
                    '<td><a class="btn btn-warning" data-id="'+room.id+'" data-action="hide-room">Hide</a></td>'+
                    '<td><a class="btn btn-primary" data-id="'+room.id+'" data-acion="update-room">Update</a></td>'+
                    '</tr>';
                $('#table_room tbody').append(html);
                alertDialog('Create Room','Success! A new room had been added','success');
                $('#close_from').click();
            }
        });

        $('#create_room').click((evt) => {
           let title = $('#title_room').val().trim();
           let desciption = $('#description_room').val().trim();
           if (title !== ''){
               socket.emit('createRoom',title,desciption);
               $('#title_room').val('');
               $('#description_room').val('');
           }
        });
    });
 };