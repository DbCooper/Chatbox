- Chat application using
    + Nodejs express framework
    + Passportjs
    + Redis
    + Sock io
    + MogoDb/Mongoose
- Feature:
    + Chat on to one.
    + Chat rooms.
    + Display user online real-time
    + Push notification.
    + Create rooms
    