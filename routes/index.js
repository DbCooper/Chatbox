var express = require('express');
var router = express.Router();
var passport = require("passport");
var userRepository = require("../src/repository/userRepository");
var RoomController = require('../src/controller/roomsController');
var ChatRoomController = require('../src/controller/chatroomController');
/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.isAuthenticated()){
    res.redirect("/rooms");
  }else{
    res.render("login",{
        success: req.flash('success')[0],
        errors: req.flash('error'),
        showRegisterForm: req.flash('showRegisterForm')[0]
    })
  }
});

router.post("/login",passport.authenticate("login-local",{
    successRedirect: '/rooms',
    failureRedirect: '/',
    failureFlash: true
}));

router.post("/register",async (req, res, next) =>{
  var credentials = {
      username: req.body.username,
      password: req.body.passwordsignup
  };
  console.log(credentials);
    if(credentials.username === '' || credentials.password === ''){
        req.flash('error', 'Missing credentials');
        req.flash('showRegisterForm', true);
        res.redirect('/');
    }else {
        var user = await userRepository.findOne({'username': new RegExp('^' + credentials.username + '$', 'i'), 'socialId': null});
        // Check if the username already exists for non-social account
        if (user){
            req.flash('error', 'Username already exists.');
            req.flash('showRegisterForm', true);
            res.redirect('/');
        }else{
          await userRepository.create(credentials);
          req.flash('success', 'Your account has been created. Please log in.');
          res.redirect("/");
        }
    }
});

// Social Authentication routes
// 1. Login via Facebook
router.get('/auth/facebook', passport.authenticate('facebook'));
router.get('/auth/facebook/callback', passport.authenticate('facebook', {
    successRedirect: '/rooms',
    failureRedirect: '/',
    failureFlash: true
}));

var isAuthenticated = function (req, res, next) {
    if(req.isAuthenticated()){

         // req.user.username==='CooperA'? next():res.redirect('/chatroom');
        next();
    }else{
        res.redirect('/');
    }
};
//rooms
router.get('/rooms',[isAuthenticated,RoomController.Listroom]);
//chat room
router.get('/chatroom',[isAuthenticated,ChatRoomController.Index]);

module.exports = router;
